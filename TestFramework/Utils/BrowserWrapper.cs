﻿using log4net;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System.Collections.Generic;
using WebDriverManager.DriverConfigs.Impl;
using WebDriverManager.Helpers;

namespace TestFramework.Utils
{
    public enum Browser
    {
        Chrome,
        Edge,
        Firefox,
        IE
    }

    public interface IBrowser
    {
        IWebDriver WebDriver { get; }
    }

    public class BrowserWrapper
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ApplicationSetUpSingleton));

        public IWebDriver Driver;
        private static bool _remote;

        public BrowserWrapper(Browser? browser, bool remote = false)
        {
            _remote = remote;
            Driver = BrowserByName.ContainsKey(browser)
                ? BrowserByName[browser].WebDriver
                : throw new WebDriverException($"No such browser name as \'{browser}\' in the browser pool");
        }
       
        public Dictionary<Browser?, IBrowser> BrowserByName => new Dictionary<Browser?, IBrowser>
        {
            {Browser.Chrome, new ChromeBrowser()},
            {Browser.Edge, new EdgeBrowser()},
            {Browser.Firefox, new FirefoxBrowser()},
            {Browser.IE, new InternetExplorerBrowser()}
        };

        private static DriverManagerWrapper DriverManager => new DriverManagerWrapper();

        public void GoToUrl(string url)
        {
            Driver.Navigate().GoToUrl(url);
            log.Debug($"Go to URL {url}");
        }

        public void RefreshPage()
        {
            Driver.Navigate().Refresh();
            log.Debug("Page Refresh");
        }

        public void Quit()
        {
            if (Driver != null)
            {
                Driver.Quit();
                Driver = null;
                log.Debug("Browser quit");
            }
        }

        private class ChromeBrowser : IBrowser
        {
            public IWebDriver WebDriver
            {
                get
                {
                    if (_remote)
                    {
                        return DriverManager.InitilizeRemoteWebDriver(new ChromeOptions());
                    }

                    DriverManager.SetUpBinaries(new ChromeConfig());
                    return new ChromeDriver();
                }
            }
        }

        private class EdgeBrowser : IBrowser
        {
            public IWebDriver WebDriver
            {
                get
                {
                    if (_remote)
                    {
                        return DriverManager.InitilizeRemoteWebDriver(new EdgeOptions());
                    }
                    DriverManager.SetUpBinaries(new EdgeConfig());
                    return new EdgeDriver();
                }
            }
        }

        private class FirefoxBrowser : IBrowser
        {
            public IWebDriver WebDriver
            {
                get
                {
                    if (_remote)
                    {
                        return DriverManager.InitilizeRemoteWebDriver(new FirefoxOptions());
                    }
                    DriverManager.SetUpBinaries(new FirefoxConfig());
                    return new FirefoxDriver();
                }
            }
        }

        private class InternetExplorerBrowser : IBrowser
        {
            public IWebDriver WebDriver
            {
                get
                {
                    if (_remote)
                    {
                        return DriverManager.InitilizeRemoteWebDriver(new InternetExplorerOptions());
                    }
                    DriverManager.SetUpBinaries(new InternetExplorerConfig(), Architecture.X32);
                    return new InternetExplorerDriver();
                }
            }
        }
    }
}
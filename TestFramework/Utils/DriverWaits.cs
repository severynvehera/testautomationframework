﻿using log4net;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;

namespace TestFramework.Utils
{
    public class DriverWaits
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(DriverWaits));

        public DriverWaits(IWebDriver driver, TimeSpan timeout)
        {
            Driver = driver;
            Wait = new WebDriverWait(driver, timeout);
        }

        private IWebDriver Driver { get; }
        private WebDriverWait Wait { get; }

        public void WaitForAlert()
        {
            WaitForCondition(ExpectedConditions.AlertIsPresent(), $"AlertIsPresent");
        }

        public void WaitForClickable(By selector)
        {
            WaitForCondition(ExpectedConditions.ElementToBeClickable(selector), $"ElementToBeClickable {selector.ToString()}");
        }

        public void WaitForInvisibilityOf(By selector)
        {
            WaitForCondition(ExpectedConditions.InvisibilityOfElementLocated(selector), $"InvisibilityOfElementLocated {selector.ToString()}");
        }

        public void WaitForPresenceOfAllElements(By selector)
        {
            WaitForCondition(ExpectedConditions.PresenceOfAllElementsLocatedBy(selector), $"PresenceOfAllElementsLocated {selector.ToString()}");
        }

        public void WaitForStalenessOf(By selector)
        {
            WaitForCondition(ExpectedConditions.StalenessOf(Driver.FindElement(selector)), $"StalenessOf {selector.ToString()}");
        }

        public void WaitForText(By selector, string text)
        {
            WaitForCondition(ExpectedConditions.TextToBePresentInElement(Driver.FindElement(selector), text), $"Text present {selector.ToString()}");
        }

        public void WaitForVisible(By selector)
        {
            WaitForCondition(ExpectedConditions.ElementIsVisible(selector), $"ElementIsVisible {selector.ToString()}");
        }

        private void WaitForCondition<TResult>(Func<IWebDriver, TResult> condition, string message)
        {
            try
            {
                Wait.Until(condition);
            }
            catch (WebDriverTimeoutException e)
            {
                log.Error($" {e.Message} {message}");
                throw;
            }
            catch (Exception e)
            {
                log.Error($" {e.Message}");
                throw;
            }
        }
    }
}

﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TestFramework.Utils
{
    public class WindowHandler
    {
        private IWebDriver Driver { get; }
        private IAlert _alert;

        public WindowHandler(IWebDriver driver)
        {
            Driver = driver;
        }

         public void MaximizeWindow()
        {
            Driver.Manage().Window.Maximize();
         //   log.Info("Maximize window");
        }

        public Screenshot TakeScreenshot()
        {
            return ((ITakesScreenshot)Driver).GetScreenshot();
        }
        #region Alert

        public IAlert Alert => _alert ?? (_alert = Driver.SwitchTo().Alert());

        public string AlertText => Alert.Text;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accept">If TRUE - accepts alert, if FALSE - dismisses.</param>
        public void CloseAlert(bool accept = true)
        {
            if (accept)
            {
                Alert.Accept();
            }
            else
            {
                Alert.Dismiss();
            }
            _alert = null;
        }

        public bool VerifyAlertText(string text)
        {
            return AlertText.Equals(text);
        }

        #endregion

        #region Window

        public List<string> AllWindows => Driver.WindowHandles.ToList();

        public string CurrentWindow => Driver.CurrentWindowHandle;

        public string InitialWindow { get; set; }

        public Dictionary<string, string> WindowNameByTitle
        {
            get
            {
                var result = new Dictionary<string, string>();

                foreach (var window in AllWindows)
                {
                    SwitchToWindow(window);
                    result.Add(Driver.Title, window);
                }
                return result;
            }
        }

        public int WindowsCount => Driver.WindowHandles.Count;

        public void CloseWindow()
        {
            Driver.Close();
        }

        public void OpenNewWindow()
        {
            throw new NotSupportedException();
        }

        public void SwitchToFirstWindow()
        {
            SwitchWindowByNumber(1);
        }

        public void SwitchToInitialWindow()
        {
            SwitchToWindow(InitialWindow);
        }

        public void SwitchToWindowByTitle(string title)
        {
            foreach (var windowTitle in WindowNameByTitle)
            {
                if (windowTitle.Key.Contains(title))
                {
                    SwitchToWindow(windowTitle.Value);
                }
            }
        }

        public void SwitchWindowByNumber(int number)
        {
            SwitchToWindow(AllWindows[number - 1]);
        }

        private void SwitchToWindow(string window)
        {
            Driver.SwitchTo().Window(window);
        }

        #endregion
    }
}

﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using System;
using System.IO;
using WebDriverManager;
using WebDriverManager.DriverConfigs;
using WebDriverManager.Helpers;

namespace TestFramework.Utils
{
    public class DriverManagerWrapper
    {
        private readonly string GridHubUrl = GlobalTools.GetValueOf("hub.url");

        private bool UseDefaultFolder => Convert.ToBoolean(GlobalTools.GetValueOf("use.default.folder"));

        public RemoteWebDriver InitilizeRemoteWebDriver(DriverOptions options)
        {
            return GridHubUrl.Contains("browserstack")
                ? new RemoteWebDriver(new Uri(GridHubUrl), SetupCapabilities(options))
                : new RemoteWebDriver(new Uri(GridHubUrl), options);
        }

        public void SetUpBinaries(IDriverConfig driverConfig, Architecture architecture = Architecture.Auto,
            string version = "Latest")
        {
            architecture = (architecture.Equals(Architecture.Auto) ? ArchitectureHelper.GetArchitecture() : architecture);
            version = version.Equals("Latest") ? driverConfig.GetLatestVersion() : version;

            var url = UrlHelper.BuildUrl(architecture.Equals(Architecture.X32) ? driverConfig.GetUrl32() : driverConfig.GetUrl64(), version);

            var driversPath = Path.Combine("Driver Binaries", driverConfig.GetName(), architecture.ToString(), version,
                driverConfig.GetBinaryName());

            var path = UseDefaultFolder
                ? Path.Combine(AppContext.BaseDirectory, driversPath)
                : Path.Combine(GlobalTools.GetValueOf("driver.binaries.folder"), driversPath);

            new DriverManager().SetUpDriver(url, path, driverConfig.GetBinaryName());
        }

        private DesiredCapabilities SetupCapabilities(DriverOptions options)
        {
            string version = "";
            string name = "";
            if (options.GetType().Equals(typeof(ChromeOptions)))
            {
                version = "70";
                name = Browser.Chrome.ToString();
            }
            else if (options.GetType().Equals(typeof(EdgeOptions)))
            {
                version = "17";
                name = Browser.Edge.ToString();
            }
            else if (options.GetType().Equals(typeof(FirefoxOptions)))
            {
                version = "63";
                name = Browser.Firefox.ToString();
            }
            else if (options.GetType().Equals(typeof(InternetExplorerOptions)))
            {
                version = "11";
                name = Browser.IE.ToString();
            }

            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.SetCapability("browser", name);
            capabilities.SetCapability("browser_version", version);
            capabilities.SetCapability("os", "Windows");
            capabilities.SetCapability("os_version", "10");
            capabilities.SetCapability("resolution", "1920x1080");
            capabilities.SetCapability("browserstack.user", "freetrial71");
            capabilities.SetCapability("browserstack.key", "ye6DQZnr81zR8wx1rqCk");

            return capabilities;
        }
    }
}

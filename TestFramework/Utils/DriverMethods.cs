﻿using log4net;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System.Collections.Generic;

namespace TestFramework.Utils
{
    public class DriverMethods
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(DriverMethods));

        public DriverMethods(IWebDriver driver)
        {
            Driver = driver;
        }

        private Actions Action => ApplicationSetUpSingleton.Instance.Action;
        private IWebDriver Driver { get; }
        private IJavaScriptExecutor JavaScript => ApplicationSetUpSingleton.Instance.JavaScript;
        private DriverWaits Waits => ApplicationSetUpSingleton.Instance.Waits;
        private WindowHandler Window => ApplicationSetUpSingleton.Instance.Window;

        public void Click(By selector)
        {
            Waits.WaitForClickable(selector);
            FindElement(selector).Click();
            log.Debug("Click");
        }

        public void ClickViaJs(By selector)
        {
            Waits.WaitForClickable(selector);
            JavaScript.ExecuteScript("arguments[0].click();", FindElement(selector));
            log.Debug("Click using JavaScript");
        }

        public void EnterText(By selector, string text, bool clear = false)
        {
            if (clear)
            {
                FindElement(selector).Clear();
                log.Debug("Clear text");
            }
            Click(selector);
            FindElement(selector).SendKeys(text);
            log.Debug($"Enter text \'{text}\'");
        }

        public void EnterTexViaJs(By selector, string text)
        {
            JavaScript.ExecuteScript($"arguments[0].value='{text}';", FindElement(selector));
            log.Debug($"Enter text \'{text}\' using JavaScript");
        }

        public string GetElementAttribute(By selector, string attribute)
        {
            log.Debug($"Get \'{attribute}\' attribute");
            return FindElement(selector).GetAttribute(attribute);
        }

        public string GetText(By selector)
        {
            log.Debug("Get element text using \'value\' attribute");
            return FindElement(selector).Text;
        }

        public IList<string> GetTextFromList(By selector)
        {
            var result = new List<string>();

            foreach (var webElement in FindElements(selector))
            {
                result.Add(webElement.Text);
            }

            return result;
        }

        public bool IsElementDisplayed(By selector)
        {
            return FindElement(selector).Displayed;
        }

        public bool IsElementExist(By selector)
        {
            return FindElements(selector).Count != 0;
        }

        public string PasteFromBuffer()
        {
            return $"{Keys.Control}v";
        }

        public void RightClick(By selector)
        {
            Action.ContextClick(FindElement(selector)).Perform();
            log.Debug("Right click");
        }

        public void ScrollIntoFooter()
        {
            JavaScript.ExecuteScript("window.scrollTo(0, document.body.scrollHeight)");
            log.Debug("Scroll Into Footer");
        }

        public void ScrollIntoView(By selector)
        {
            JavaScript.ExecuteScript("arguments[0].scrollIntoView();", FindElement(selector));
            log.Debug("Scroll Into View");
        }

        private IWebElement FindElement(By selector)
        {
            try
            {
                log.Debug($"Find Element  {selector.ToString()}");
                return Driver.FindElement(selector);
            }
            catch (NoSuchElementException e)
            {
                log.Error(e.Message);
                throw;
            }
            catch (UnhandledAlertException e)
            {
                Window.CloseAlert();
                log.Error($"{e.Message}");
                throw;
            }
        }

        private IReadOnlyCollection<IWebElement> FindElements(By selector)
        {
            log.Debug($"Find Elements{selector.ToString()}");
            return Driver.FindElements(selector);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Xml;

namespace TestFramework.Utils
{
    public static class GlobalTools
    {
        /// <summary>
        /// Gets the path to solution root folder 
        /// </summary>
        public static string SolutionRoot
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                path = Path.GetDirectoryName(path);

                path = path.Replace("\\Tests\\bin\\Release", ""); // Change it to the folder that you want. 
                path = path.Replace("\\Tests\\bin\\Debug", "");
                path = path.Replace("\\Tests\\bin\\Dev", "");
                path = path.Replace("\\Tests\\bin\\x86\\Release", "");
                path = path.Replace("\\Tests\\bin\\x86\\Debug", "");
                path = path.Replace("\\Tests\\bin\\x86\\Dev", "");

                return path;
            }
        }

        /// <summary>
        /// Creates XML document 'environment.xml' in allure-results folder
        /// </summary>
        /// <param name="browser">Name of browser</param>
        /// <param name="baseUrl"></param>
        public static void CreateEnvironmentXml(Dictionary<string,string> envParams)
        {
            string path = Path.Combine(SolutionRoot, @"Tests\bin\Debug", $@"{envParams["Browser"]}_TestResults\allure-results");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            using (XmlWriter writer = XmlWriter.Create($@"{path}\environment.xml"))
            {
                writer.WriteStartElement("environment");
                foreach (var envParam in envParams)
                {
                    writer.WriteStartElement("parameter");
                    writer.WriteElementString("key", envParam.Key);
                    writer.WriteElementString("value", envParam.Value);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.Flush();
            }
        }

        /// <summary>
        /// Retrieves the value from the App.config.
        /// </summary>
        /// <param name="key">The name of an variable in the App.config.</param>
        public static string GetValueOf(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        public static Browser ParseBrowserName(string browser)
        {
            return (Browser)Enum.Parse(typeof(Browser), browser, true);
        }
    }
}

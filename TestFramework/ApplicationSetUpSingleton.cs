﻿using log4net;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Threading;
using TestFramework.Utils;

namespace TestFramework
{
    /// <summary>
    /// 
    /// </summary>
    public class ApplicationSetUpSingleton
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ApplicationSetUpSingleton));
        public TimeSpan ExplicitWaitTimeout { get; set; } = TimeSpan.FromSeconds(20);
        private static readonly object syncLock = new object();

        private static volatile ApplicationSetUpSingleton _instance;

        private Browser? _browser;
        private bool _remote;
        //private  TestParameters testParameters;

        private ApplicationSetUpSingleton(Browser? browser, bool remote = false)
        {
            _browser = browser;
            _remote = remote;
            BrowserByThreadId = new Dictionary<int, BrowserWrapper>();
        }

        public static ApplicationSetUpSingleton Instance => Initilize(null);

        /// <summary>
        /// Initializes local or remote Browser instance
        /// </summary>
        /// <param name="browser">Browser name</param>
        /// <param name="remote">If TRUE - Initializes remote WebDriver instance , if FALSE - local</param>
        public static ApplicationSetUpSingleton Initilize(Browser? browser, bool remote = false)
        {
            if (_instance == null)
            {
                lock (syncLock)
                {
                    if (_instance == null)
                    {
                        if (browser == null)
                        {
                            browser = Utils.Browser.Chrome;
                        }
                        _instance = new ApplicationSetUpSingleton(browser, remote);
                        _instance.Browser = _instance.GetBrowser();
                        _instance.Window = new WindowHandler(_instance.Browser.Driver);
                        _instance.Methods = new DriverMethods(_instance.Browser.Driver);
                        _instance.Waits = new DriverWaits(_instance.Browser.Driver, _instance.ExplicitWaitTimeout);
                        _instance.Action = new Actions(_instance.Browser.Driver);
                        _instance.JavaScript = ((IJavaScriptExecutor)_instance.Browser.Driver);
                    }
                }
            }
            return _instance;
        }

        public BrowserWrapper Browser { get; set; }

        public WindowHandler Window { get; private set; }

        public DriverMethods Methods { get; private set; }

        public DriverWaits Waits { get; private set; }

        public Actions Action { get; private set; }

        public IJavaScriptExecutor JavaScript { get; private set; }

        public Dictionary<int, BrowserWrapper> BrowserByThreadId;
    
        private BrowserWrapper GetBrowser()
        {
            if (!BrowserByThreadId.ContainsKey(Thread.CurrentThread.ManagedThreadId))
            {
                BrowserByThreadId.Add(Thread.CurrentThread.ManagedThreadId, new BrowserWrapper(_browser, _remote));
            }
            return BrowserByThreadId[Thread.CurrentThread.ManagedThreadId];
            //return new BrowserWrapper(_browser, _remote);
        }

        public static void TearDown()
        {
            if (_instance != null)
            {
                foreach (var browsersKey in _instance.BrowserByThreadId.Keys)
                {
                    if (_instance.BrowserByThreadId[browsersKey] != null)
                    {
                        _instance.BrowserByThreadId[browsersKey].Quit();
                        _instance.BrowserByThreadId.Remove(browsersKey);
                    }
                }
                // _instance.Browser.Quit();
                _instance = null;
                log.Debug("Application tear down");
            }
        }
    }
}

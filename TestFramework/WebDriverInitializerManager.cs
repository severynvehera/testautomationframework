﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using WebDriverManager;
using WebDriverManager.DriverConfigs;
using WebDriverManager.DriverConfigs.Impl;
using WebDriverManager.Helpers;
using WebDriverManager.Services.Impl;

namespace TestFramework
{
    public class WebDriverInitializerManager
    {
        private static WebDriverInitializerManager _instance;
        private IWebDriver _driver;
        private WebDriverInitializerManager() { }

        public static WebDriverInitializerManager Instance => _instance ?? (_instance = new WebDriverInitializerManager());

        public IWebDriver Driver => _driver ?? (_driver = Initialize(Config.GetValueOf("browser")));

        private IWebDriver Initialize(string browserName)
        {
            switch (browserName)
            {
                case "firefox":
                case "ff":
                    Setup(new FirefoxConfig(), Architecture.X64);
                    return IsRemote() ? new RemoteWebDriver(new Uri(Config.GetValueOf("hub.url")), new FirefoxOptions()) : new FirefoxDriver();
                case "internet explorer":
                case "ie":
                    Setup(new InternetExplorerConfig(), Architecture.X64);
                    return IsRemote() ? new RemoteWebDriver(new Uri(Config.GetValueOf("hub.url")), new InternetExplorerOptions()) : new InternetExplorerDriver();

                case "edge":
                    Setup(new EdgeConfig(), Architecture.X64);
                    return IsRemote() ? new RemoteWebDriver(new Uri(Config.GetValueOf("hub.url")), new EdgeOptions()) : new EdgeDriver();
                default:
                    Setup(new ChromeConfig(), Architecture.X64);
                    return IsRemote() ? new RemoteWebDriver(new Uri(Config.GetValueOf("hub.url")), new ChromeOptions()) : new ChromeDriver();

            }
        }

        private void Setup(IDriverConfig driverConfig, Architecture architecture = Architecture.Auto, string version = "Latest")
        {
            version = version.Equals("Latest") ? driverConfig.GetLatestVersion() : version;

            var url = UrlHelper.BuildUrl(architecture.Equals(Architecture.X32) ? driverConfig.GetUrl32() : driverConfig.GetUrl64(), version);

            // Structure of drivers storage. Example: .../Driver Binaries/Chrome/x64/chromediver.exe
            var driversStorage = Path.Combine("Driver Binaries", driverConfig.GetName(), architecture.ToString(), driverConfig.GetBinaryName());
            var path = UseDefaultFolder() ? Path.Combine(AppContext.BaseDirectory, driversStorage) : Path.Combine(Config.GetValueOf("driver.binaries.folder"), driversStorage);

            new DriverManager().SetUpDriver(url, path, driverConfig.GetBinaryName());
        }

        private bool IsRemote()
        {
            return Convert.ToBoolean(Config.GetValueOf("run.on.hub"));
        }

        private bool UseDefaultFolder()
        {
            return Convert.ToBoolean(Config.GetValueOf("use.default.folder"));
        }
    }
}

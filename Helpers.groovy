void runTest(String browser, String fixture, String remote = false){
    try{
        if ("${fixture}" == 'All tests') {
            bat "\"${nubit_console}\" \"${tests_dll}\" --params \"browser=${browser};remote=${remote}\" /result:\"${browser}_${nunit_results_xml}\""
        } else {
            bat "\"${nubit_console}\" \"${tests_dll}\" --where \"class == Tests.TestClasses.${fixture}\" --params \"browser=${browser};remote=${remote}\" /result:\"${browser}_${nunit_results_xml}\""
        }
    } finally{
        step([$class: 'XUnitBuilder',

         thresholds: [ 
             skipped(failureThreshold: '0'), 
             failed(failureThreshold: '0') 
             ],

         tools: [
             [$class: 'NUnit3TestType',
               deleteOutputFiles: true, 
               failIfNotNew: true, 
               pattern: "${browser}_${nunit_results_xml}",
               skipNoTestFiles: false, 
               stopProcessingIfError: true]
             ]
        ])
        
        // allure([
        //  results: [[path: 'allure-results'],]
        //  ])
         bat "\"${allure_bat}\" generate allure-results -c -o allure-report"

         publishHTML([
                  allowMissing: false, 
                  alwaysLinkToLastBuild: false, 
                  keepAll: true, 
                  reportDir: "./allure-report", 
                  reportFiles: 'index.html', 
                  reportName: "${browser}Reports", 
                  reportTitles: ''
                  ])
    }
}

void createTestResultsDirectories(){
    dir("${debug_directory}"){
        browsers.each {
            bat "mkdir ${it}_TestResults"
        }
    }
}

void cleanUpWorkspace(String status){
    step([$class: 'WsCleanup', deleteDirs: true, patterns: [[pattern: "**/*@tmp", type: 'INCLUDE'], [pattern: '**/generatedJUnitFiles', type: 'INCLUDE']]])
    currentBuild.result = "${status}"
    if (status == 'FAILURE' || status == 'ABORTED'){
       error('Something wrong')
    }
}
return this


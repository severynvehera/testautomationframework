﻿using Pages.PageObjects.Pages;
using Pages.Pages.Blocks;
using Pages.Pages.Containers;
using Pages.Pages.Dialogs;
using Pages.Pages.Dialogs.Delete;
using TestFramework;
using TestFramework.Utils;

namespace Pages
{
    public class PagesSingleton
    {
        private static PagesSingleton _instance;

        /*
                private LoginPage _login;
                private LogOutPage _logout;
                private AdminPage _admin;
                private CreateNewFolderPage _createNewFolderPage;
                private CabinetPage _cabinet;
                private AdvancedSearchPage _advancedSearch;
                private Folder _folderContainer;
                private SearchResults _searchResultsContainer;
                private TopBar _topBar;
                private NavigationBar _navigationBar;
                private HeaderSearch _headerSearch;
                private NavigationPane _navigationPane;
                private ContainerPowerBar _containerPowerBar;
                private ItemDetailPane _itemDetailPane;
                private IDelete _deleteDialog;
                private IDelete _deletePopUp;
                private RenameDialog _renameDialog;
                private Stub _stub;
        */
        private PagesSingleton()
        {
        }

        public static PagesSingleton Instance => _instance ?? (_instance = new PagesSingleton());

        public AdminPage Admin => new AdminPage();

        public AdvancedSearchPage AdvancedSearch => new AdvancedSearchPage();

        public CabinetPage Cabinet => new CabinetPage();

        public ContainerPowerBar ContainerPowerBar => new ContainerPowerBar();

        public CreateNewFolderPage CreateNewFolderPage => new CreateNewFolderPage();

        public IDelete DeleteDialog => new DeleteDialog();

        public IDelete DeletePopUp => new DeletePopUp();

        public Folder FolderContainer => new Folder();

        public HeaderSearch HeaderSearch => new HeaderSearch();

        public ItemDetailPane ItemDetailPane => new ItemDetailPane();

        public LoginPage Login => new LoginPage();

        public LogOutPage Logout => new LogOutPage();

        public NavigationBar NavigationBar => new NavigationBar();

        public NavigationPane NavigationPane => new NavigationPane();

        public RenameDialog RenameDialog => new RenameDialog();

        public SearchResults SearchResultsContainer => new SearchResults();

        public TopBar TopBar => new TopBar();

        public BrowserWrapper Browser() => ApplicationSetUpSingleton.Instance.Browser;
        public DriverMethods Methods() => ApplicationSetUpSingleton.Instance.Methods;
        public DriverWaits Waits() => ApplicationSetUpSingleton.Instance.Waits;
        public WindowHandler Window() => ApplicationSetUpSingleton.Instance.Window;


        /*
        public  LoginPage Login => _login ?? (_login =  new LoginPage());

        public  LogOutPage Logout => _logout ?? (_logout = new LogOutPage());

        public  AdminPage Admin => _admin ?? (_admin = new AdminPage());

        public  CreateNewFolderPage CreateNewFolderPage => _createNewFolderPage ?? (_createNewFolderPage =  new CreateNewFolderPage());

        public  CabinetPage Cabinet => _cabinet ?? (_cabinet =  new CabinetPage());

        public  AdvancedSearchPage AdvancedSearch => _advancedSearch ?? (_advancedSearch =  new AdvancedSearchPage());

        public  Folder FolderContainer => _folderContainer ?? (_folderContainer = new Folder());

        public  SearchResults SearchResultsContainer =>  _searchResultsContainer ?? (_searchResultsContainer =  new SearchResults());
     
        public  TopBar TopBar => _topBar ?? (_topBar =  new TopBar());

        public  NavigationBar NavigationBar => _navigationBar ?? (_navigationBar =  new NavigationBar());

        public  HeaderSearch HeaderSearch => _headerSearch ?? (_headerSearch =  new HeaderSearch());

        public  NavigationPane NavigationPane => _navigationPane ?? (_navigationPane =  new NavigationPane());

        public  ContainerPowerBar ContainerPowerBar => _containerPowerBar ?? (_containerPowerBar =  new ContainerPowerBar());

        public  ItemDetailPane ItemDetailPane => _itemDetailPane ?? (_itemDetailPane =  new ItemDetailPane());

        public  IDelete DeleteDialog => _deleteDialog ?? (_deleteDialog =  new DeleteDialog());

        public  IDelete DeletePopUp => _deletePopUp ?? (_deletePopUp =  new DeletePopUp());

        public  RenameDialog RenameDialog => _renameDialog ?? (_renameDialog =  new RenameDialog());

        public Stub Stub => _stub ?? (_stub =  new Stub());
       */

    }
}

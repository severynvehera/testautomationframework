﻿using TestFramework;
using TestFramework.Utils;

namespace Pages.Pages
{
    public abstract class APage<T>
    {
        protected DriverMethods methods;
        protected DriverWaits waits;
        protected WindowHandler window;
        protected BrowserWrapper browser;
        protected PagesSingleton pages;
        protected T selectors;

        public T Selectors
        {
            get { return selectors; }
        }

        protected APage(T t)
        {
            methods = ApplicationSetUpSingleton.Instance.Methods;
            waits = ApplicationSetUpSingleton.Instance.Waits;
            window = ApplicationSetUpSingleton.Instance.Window;
            pages = PagesSingleton.Instance;
            browser = ApplicationSetUpSingleton.Instance.Browser;
            selectors = t;
        }

        public abstract void WaitForLoad();
    }
}

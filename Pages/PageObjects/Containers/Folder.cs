﻿using OpenQA.Selenium;

namespace Pages.Pages.Containers
{
    public class Folder : AContainer
    {
        public Folder()
        {
        }
    }

    public static class FolderPageSelectors
    {
        public static readonly By ListViewById = By.Id("listView");
    }
}

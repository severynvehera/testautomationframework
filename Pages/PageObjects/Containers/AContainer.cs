﻿using OpenQA.Selenium;
using Pages.Pages.Blocks;
using Pages.Pages.OptionsMenu;

namespace Pages.Pages.Containers
{
    public abstract class AContainer : APage<ContainerSelectors>
    {
        public AContainer(bool wait = true) : base(new ContainerSelectors())
        {
            if (wait)
            {
                WaitForLoad();
            }
        }

        public string EmptyMessageText => methods.GetText(Selectors.EmptyMessageByCss);

        public ContainerPowerBar PowerBar { get; protected set; }

        public bool CheckIndexing(string itemName)
        {
            if (!methods.IsElementExist(Selectors.ItemNameByXPath(itemName)))
            {
                browser.RefreshPage();
                WaitForLoad();
            }

            return methods.IsElementExist(Selectors.ItemNameByXPath(itemName));
        }

        public ItemDetailPane ClickCheckbox(string itemName)
        {
            waits.WaitForVisible(Selectors.ItemRowByXPath(itemName));
            methods.ScrollIntoView(Selectors.ItemRowByXPath(itemName));

            waits.WaitForClickable(Selectors.ItemRowByXPath(itemName));
            //Click(ContainerSelectors.ItemRowByXPath(itemName));
            methods.Click(Selectors.CheckboxByXPath(itemName));
            return new ItemDetailPane();
        }

        public bool IsContainerEmpty()
        {
            return methods.IsElementExist(Selectors.EmptyMessageByCss);
        }

        public bool IsItemExist(string name)
        {
            return methods.GetText(Selectors.ItemRowByXPath(name)) != null;
        }

        public Folder Open(string itemName)
        {
            waits.WaitForVisible(Selectors.ItemNameByXPath(itemName));
            methods.Click(Selectors.ItemNameByXPath(itemName));
            return new Folder();
        }

        public ContainerMenu OpenContainerMenu()
        {
            methods.Click(Selectors.ContainerNameById);
            return new ContainerMenu();
        }

        public ContextMenu OpenContextMenu(string itemName)
        {
            methods.RightClick(Selectors.ItemRowByXPath(itemName));
            return new ContextMenu();
        }

        public sealed override void WaitForLoad()
        {
            //            WaitForPresenceOfAllElements(ContainerSelectors.LoadingMessageByXpath);
            waits.WaitForInvisibilityOf(Selectors.LoadingMessageByXpath);
        }
    }

    public class ContainerSelectors
    {
        public readonly By ContainerNameById = By.Id("containerName");

        public readonly By EmptyMessageByCss = By.CssSelector(".lvEmptyMsg p");

        public readonly By ItemRowByClass = By.ClassName("lvRow");

        public readonly By LoadingMessageByXpath = By.XPath("//*[contains(text(),\"items...\")]");

        public By CheckboxByXPath(string itemName)
        {
            return By.XPath($"//tr[contains(@data-itemspec,\'{itemName}\')]//td//div");
        }

        public By ItemNameByXPath(string itemName)
        {
            return By.XPath($"//tr[contains(@data-itemspec,\'{itemName}\')]//span[@class = \'docImg app-fld\']");
        }

        public By ItemRowByXPath(string itemName)
        {
            return By.XPath($"//tr[contains(@data-itemspec,\'{itemName}\')]");
        }
    }
}

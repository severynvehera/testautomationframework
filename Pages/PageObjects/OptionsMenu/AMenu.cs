﻿using OpenQA.Selenium;

namespace Pages.Pages.OptionsMenu
{
    public abstract class AMenu<T> : APage<T> where T : AMenuItems
    {
        protected AMenu(T t) : base(t)
        {
        }

        public void SelectItem(IMenuItems menuItem)
        {
            methods.Click(menuItem.Selector);
        }
    }
    public abstract class AMenuItems : IMenuItems
    {
        public string Name { get; }
        public By Selector { get; }

        protected AMenuItems() {}
        protected AMenuItems(string name, By selector)
        {
            Name = name;
            Selector = selector;
        }
    }
}

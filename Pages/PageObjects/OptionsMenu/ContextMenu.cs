﻿using OpenQA.Selenium;

namespace Pages.Pages.OptionsMenu
{
    public class ContextMenu : AMenu<ContextMenuItems>
    {
        public ContextMenu(bool wait = true) : base(new ContextMenuItems())
        {
            if (wait)
            {
                WaitForLoad();
            }
        }

        public override void WaitForLoad()
        {
            waits.WaitForPresenceOfAllElements(ContextMenuItems.Rename.Selector);
        }
    }

    public class ContextMenuItems : AMenuItems
    {
        public static readonly ContextMenuItems Rename = new ContextMenuItems("Rename", By.XPath("//*[text() = \'Rename\']"));

        public ContextMenuItems() { }
        private ContextMenuItems(string name, By selector) : base(name, selector) { }

    }

    

   
}

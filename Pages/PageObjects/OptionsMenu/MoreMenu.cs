﻿using OpenQA.Selenium;

namespace Pages.Pages.OptionsMenu
{
    public class MoreMenu : AMenu<MoreMenuItems>
    {
        public MoreMenu(bool wait = true) : base(new MoreMenuItems())
        {
            if (wait)
            {
                WaitForLoad();
            }
        }

        public override void WaitForLoad()
        {
            waits.WaitForPresenceOfAllElements(MoreMenuItems.Rename.Selector);
        }
    }

    public class MoreMenuItems : AMenuItems
    {
        public static readonly MoreMenuItems Delete = new MoreMenuItems("Delete", By.ClassName("itemDelete"));
        public static readonly MoreMenuItems Rename = new MoreMenuItems("Rename", By.ClassName("itemRename"));

        public MoreMenuItems() { }
        private MoreMenuItems(string name, By selector) : base(name, selector) { }
    }
}

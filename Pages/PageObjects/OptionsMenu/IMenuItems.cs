﻿using OpenQA.Selenium;

namespace Pages.Pages.OptionsMenu
{
    public interface IMenuItems
    {
        string Name { get; }

        By Selector { get; }
    }
}

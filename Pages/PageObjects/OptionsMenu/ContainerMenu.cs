﻿using OpenQA.Selenium;

namespace Pages.Pages.OptionsMenu
{
    public class ContainerMenu : AMenu<ContainerMenuItems>
    {
        public ContainerMenu(bool wait = true) : base(new ContainerMenuItems())
        {
            if (wait)
            {
                WaitForLoad();
            }
        }

        public override void WaitForLoad()
        {
            waits.WaitForPresenceOfAllElements(ContainerMenuItems.DeleteFolder.Selector);
        }
    }

    public class ContainerMenuItems : AMenuItems
    {
        public static readonly ContainerMenuItems DeleteFolder = new ContainerMenuItems("Delete folder", By.ClassName("folderDelete"));

        public ContainerMenuItems() { }
        private ContainerMenuItems(string name, By selector) : base(name, selector) { }
    }
}

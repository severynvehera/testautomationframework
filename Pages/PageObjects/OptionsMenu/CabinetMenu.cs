﻿using OpenQA.Selenium;

namespace Pages.Pages.OptionsMenu
{
    public class CabinetMenu : AMenu<CabinetMenuItems>
    {
        public CabinetMenu(bool wait = true) : base(new CabinetMenuItems())
        {
            if (wait)
            {
                WaitForLoad();
            }
        }


        public override void WaitForLoad()
        {
            waits.WaitForPresenceOfAllElements(CabinetMenuItems.AddFolder.Selector);
        }
    }

    public class CabinetMenuItems :AMenuItems
    {
        public static readonly CabinetMenuItems AddFolder = new CabinetMenuItems("Add folder", By.CssSelector("div.menuBox li[data-val = \"newFolder\"]"));

        public CabinetMenuItems() {}
        private CabinetMenuItems(string name, By selector) : base(name, selector) {}

       
    }

}

﻿using OpenQA.Selenium;
using Pages.Pages;

namespace Pages.PageObjects.Pages
{
    public class LogOutPage : APage<LogOutPageSelectors>
    {
        public LogOutPage(bool wait = true) : base(new LogOutPageSelectors())
        {
            if (wait)
            {
                WaitForLoad();
            }
        }

        public LoginPage OpenLoginPage()
        {
            methods.Click(Selectors.ClickHereLinkByCss);
            
            return new LoginPage();
        }

        public sealed override void WaitForLoad()
        {
            waits.WaitForPresenceOfAllElements(Selectors.ClickHereLinkByCss);
        }
    }

    public class LogOutPageSelectors
    {
        public readonly By ClickHereLinkByCss = By.CssSelector("#msgspace a");
    }
}

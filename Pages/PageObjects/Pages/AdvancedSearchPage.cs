﻿using OpenQA.Selenium;
using Pages.Pages;

namespace Pages.PageObjects.Pages
{
    public class AdvancedSearchPage : APage<AdvancedSearchPageSelectors>
    {
        public AdvancedSearchPage(bool wait = true) : base(new AdvancedSearchPageSelectors())
        {
            if (wait)
            {
                WaitForLoad();
            }
        }

        public void SearchDocumentById()
        {

            methods.EnterText(Selectors.DocumentIdFieldById, methods.PasteFromBuffer());
            methods.Click(Selectors.AdvancedSearchButtonById);
        }

        public void ShowMoreOptions()
        {
            methods.ScrollIntoFooter();
            methods.Click(Selectors.ShowMoreOptionsByCss);

            waits.WaitForClickable(Selectors.DeleteItemsOnlyById);
            methods.ScrollIntoView(Selectors.DeleteItemsOnlyById);
            methods.Click(Selectors.DeleteItemsOnlyById);
        }

        public sealed override void WaitForLoad()
        {
            waits.WaitForPresenceOfAllElements(Selectors.DocumentIdFieldById);
        }
    }

    public class AdvancedSearchPageSelectors
    {
        public readonly By AdvancedSearchButtonById = By.Id("advanced-search-button");

        public readonly By DeleteItemsOnlyById = By.Id("prop15");

        public readonly By DocumentIdFieldById = By.Id("prop999");

        public readonly By ShowMoreOptionsByCss = By.CssSelector("#mo-button span");
    }
}

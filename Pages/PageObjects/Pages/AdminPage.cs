﻿using OpenQA.Selenium;
using Pages.Pages;
using Pages.Pages.Blocks;

namespace Pages.PageObjects.Pages
{
    public class AdminPage : APage<AdminPageSelectors>
    {
        public AdminPage(bool wait = true) : base(new AdminPageSelectors())
        {
            if (wait)
            {
                WaitForLoad();
            }
        }

        public LogOutPage LogOut()
        {
           methods.Click(Selectors.LogOut);
            return new LogOutPage();
        }

        public TopBar GoHome()
        {
            methods.Click(Selectors.Home);
            return new TopBar();
        }

        public sealed override void WaitForLoad()
        {
            waits.WaitForClickable(Selectors.LogOut);
        }
    }

    public class AdminPageSelectors
    {
        public readonly By LogOut = By.XPath("//a[text() = 'Log Out']");

        public readonly By Home = By.XPath("//a[contains(text(), 'Home')]");
    }
}

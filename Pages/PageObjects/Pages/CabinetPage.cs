﻿using OpenQA.Selenium;
using Pages.Pages;
using Pages.Pages.OptionsMenu;

namespace Pages.PageObjects.Pages
{
    public class CabinetPage : APage<CabinetPageSelectors>
    {
        public CabinetPage(bool wait = true) : base(new CabinetPageSelectors())
        {
            if (wait)
            {
                WaitForLoad();
            }
            
        }

        public CabinetMenu OpenCabinetOptions()
        {
            methods.Click(Selectors.CabinetOptionsById);
            return new CabinetMenu();
        }

        public sealed override void WaitForLoad()
        {
            waits.WaitForPresenceOfAllElements(Selectors.HomeTableById);
        }
    }

    public class CabinetPageSelectors
    {
        public readonly By CabinetOptionsById = By.Id("RndBtn1");

        public readonly By HomeTableById = By.Id("homeTable");
    }
}

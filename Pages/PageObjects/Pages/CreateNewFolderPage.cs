﻿using OpenQA.Selenium;
using Pages.Pages;

namespace Pages.PageObjects.Pages
{
    public class CreateNewFolderPage : APage<CreateNewFolderPageSelectors>
    {
        public CreateNewFolderPage(bool wait = true) : base(new CreateNewFolderPageSelectors())
        {
            if (wait)
            {
                WaitForLoad();
            }
        }

        public void Cancel()
        {
           methods.Click(Selectors.CancelButtonById);
        }

        public void CreateNewFolder(string name)
        {
            methods.EnterText(Selectors.FolderNameFieldById, name, true);
            methods.Click(Selectors.ContinueButtonById);
            waits.WaitForStalenessOf(Selectors.PageTitleById);
        }

        public sealed override void WaitForLoad()
        {
            waits.WaitForPresenceOfAllElements(Selectors.ContinueButtonById);
        }
    }

    public class CreateNewFolderPageSelectors
    {
        public readonly By CancelButtonById = By.Id("cancelButton");

        public readonly By ContinueButtonById = By.Id("closeButton");

        public readonly By FolderNameFieldById = By.Id("docname");

        public readonly By PageTitleById = By.ClassName("pageTitle");
    }
}

﻿using OpenQA.Selenium;
using Pages.Pages;
using SeleniumExtras.WaitHelpers;

namespace Pages.PageObjects.Pages
{
    public class LoginPage : APage<LoginPageSelectors>
    {
        public LoginPage(bool wait = true):  base(new LoginPageSelectors())
        {
            if (wait)
            {
                WaitForLoad();
            }
        }

        public string GetErrorMessage()
        {
            return methods.GetText(Selectors.LoginErrorMessage);
        }

        public void LoginUser(string username, string password = "rewards4", bool waitForTopBar = true)
        {
            methods.EnterText(Selectors.UserNameField, username, true);
            methods.EnterText(Selectors.PasswordField, password, true);
            methods.Click(Selectors.LoginButton);

            if (waitForTopBar)
            {
               pages.TopBar.WaitForLoad();
            }

        }

        public void WaitForErrorMessage()
        {
            waits.WaitForText(Selectors.LoginErrorMessage, " ");
        }

        public sealed override void WaitForLoad()
        {
            waits.WaitForClickable(Selectors.UserNameField);
        }
    }

    public class LoginPageSelectors
    {
        public readonly By LoginButton = By.Id("loginBtn");

        public readonly By LoginErrorMessage = By.Id("loginResponse");

        public readonly By PasswordField = By.Id("password");

        public readonly By UserNameField = By.Id("username");
    }
}

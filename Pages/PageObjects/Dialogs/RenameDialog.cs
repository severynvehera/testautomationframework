﻿using OpenQA.Selenium;

namespace Pages.Pages.Dialogs
{
    public class RenameDialog : APage<RenameDialogSelectors>
    {
        public RenameDialog(bool wait = true) : base(new RenameDialogSelectors())
        {
            if (wait)
            {
                WaitForLoad();
            }
        }

        public void Cancel()
        {
           methods.Click(Selectors.CancelButtonById);
        }

        public void NewName(string newName)
        {
            methods.EnterTexViaJs(Selectors.RenameFieldById, newName);
            //EnterText(RenameDialogSelectors.RenameFieldById, newName, true);
            methods.Click(Selectors.OkButtonByCss);
            waits.WaitForInvisibilityOf(Selectors.RenameDialogBlockById);
        }

        public sealed override void WaitForLoad()
        {
            waits.WaitForPresenceOfAllElements(Selectors.CancelButtonById);
        }
    }

    public class RenameDialogSelectors
    {
        public readonly By CancelButtonById = By.Id("cancelButton");

        public readonly By OkButtonByCss = By.CssSelector(".rndBtn.blueBtn.wide");

        public readonly By RenameDialogBlockById = By.Id("renameDlg");

        public readonly By RenameFieldById = By.Id("newName");
    }
}

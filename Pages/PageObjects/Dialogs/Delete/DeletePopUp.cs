﻿using OpenQA.Selenium;

namespace Pages.Pages.Dialogs.Delete
{
    public class DeletePopUp : APage<DeletePopUpSelectors>, IDelete
    {
        private const string Title = "Delete Folder";
        public DeletePopUp(bool wait = true) : base(new DeletePopUpSelectors())
        {
            window.SwitchToWindowByTitle(Title);
            if (wait)
            {
                WaitForLoad();
            }
        }

        public void Cancel()
        {
            methods.Click(Selectors.CancelButtonByName);

            window.SwitchToInitialWindow();
        }

        public void Delete()
        {
            //TODO add IsFolderEmpty() check;
            //Click(DeletePopUpSelectors.DeleteContentRadioButtonById);

            methods.Click(Selectors.ConfirmationFieldById);
            methods.EnterText(Selectors.ConfirmationFieldById, "DELETE", true);
            methods.Click(Selectors.OkButtonByName);

            window.SwitchToInitialWindow();
        }

        public void Unfile()
        {
            methods.Click(Selectors.UnfileContentRadioButtonById);
            methods.Click(Selectors.ConfirmationFieldById);
            methods.EnterText(Selectors.ConfirmationFieldById, "UNFILE", true);
            methods.Click(Selectors.OkButtonByName);

            window.SwitchToInitialWindow();
        }

        public sealed override void WaitForLoad()
        {
            waits.WaitForPresenceOfAllElements(Selectors.CancelButtonByName);
        }
    }

    public class DeletePopUpSelectors
    {
        public readonly By CancelButtonByName = By.Name("cancelButton");

        public readonly By ConfirmationFieldById = By.Id("delFolderDlgConfirmation");

        public readonly By DeleteContentRadioButtonById = By.Id("delDocs");

        public readonly By OkButtonByName = By.Name("okButton");

        public readonly By UnfileContentRadioButtonById = By.Id("unfileDocs");
    }
}

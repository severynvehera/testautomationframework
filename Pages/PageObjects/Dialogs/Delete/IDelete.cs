﻿namespace Pages.Pages.Dialogs.Delete
{
    public interface IDelete
    {
        void Cancel();

        void Delete();

        void Unfile();
    }
}

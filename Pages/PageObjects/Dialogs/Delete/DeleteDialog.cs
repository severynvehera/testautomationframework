﻿using OpenQA.Selenium;

namespace Pages.Pages.Dialogs.Delete
{
    public class DeleteDialog : APage<DeleteDialogSelectors>, IDelete
    {
        public DeleteDialog(bool wait = true) : base(new DeleteDialogSelectors())
        {
            if (wait)
            {
                WaitForLoad();
            }
        }

        public void ClickOkButton()
        {
            methods.Click(Selectors.OkButtonByCss);
        }

        public void Cancel()
        {
           methods.Click(Selectors.CancelButtonByCss);
        }

        public void Delete()
        {
            methods.Click(Selectors.DeleteContentRadioButtonById);
            methods.Click(Selectors.ConfirmationFieldById);
            methods.EnterText(Selectors.ConfirmationFieldById, "DELETE", true);
            ClickOkButton();

            waits.WaitForInvisibilityOf(Selectors.DeleteDialogBlockByCss);
        }

        public void Unfile()
        {
            methods.Click(Selectors.UnfileContentRadioButtonById);
            methods.EnterText(Selectors.ConfirmationFieldById, "UNFILE", true);
            ClickOkButton();
        }

        public sealed override void WaitForLoad()
        {
            waits.WaitForPresenceOfAllElements(Selectors.CancelButtonByCss);
        }
    }

    public class DeleteDialogSelectors
    {
        public readonly By CancelButtonByCss = By.CssSelector("div#deleteDlg a");

        public readonly By ConfirmationFieldById = By.Id("multiDeleteConfirmation");

        public readonly By DeleteContentRadioButtonById = By.Id("multiDelDeleteDocs");

        public readonly By DeleteDialogBlockByCss = By.CssSelector("div#deleteDlg");

        public readonly By OkButtonByCss = By.CssSelector(".rndBtn.blueBtn.wide");

        public readonly By UnfileContentRadioButtonById = By.Id("multiDelUnfileDocs");
    }
}

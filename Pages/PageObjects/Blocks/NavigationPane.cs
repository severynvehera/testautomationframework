﻿using OpenQA.Selenium;
using Pages.PageObjects.Pages;
using Pages.Pages.Containers;

namespace Pages.Pages.Blocks
{
    public class NavigationPane : APage<NavigationPaneSelectors>
    {
        public NavigationPane(bool wait = true) : base(new NavigationPaneSelectors())
        {
            if (wait)
            {
                WaitForLoad();
            }
        }

        public CreateNewFolderPage AddTopLevelFolder(string repository, string cabinet)
        {
            Expand(repository, cabinet, "Folders");
            waits.WaitForVisible(Selectors.AddFolderByClass);
            methods.ScrollIntoView(Selectors.AddFolderByClass);

            methods.Click(Selectors.AddFolderByClass);
            return new CreateNewFolderPage();
        }

        public void Collapse(params string[] nameList)
        {
            foreach (var name in nameList)
            {
                waits.WaitForPresenceOfAllElements(Selectors.LinkByXPath(name));
                methods.Click(Selectors.OpenArrowByXPath(name));
            }
        }

        public void Expand(params string[] nameList)
        {
            foreach (var name in nameList)
            {
                waits.WaitForPresenceOfAllElements(Selectors.LinkByXPath(name));
                // TODO remove if statement.
                if (IsExpanded(name)) continue;
                methods.Click(Selectors.ClosedArrowByXPath(name));
            }
        }

        public void ExpandFolders(string repository, string cabinet, params string[] folders)
        {
            Expand(repository, cabinet, "Folders");

            foreach (var folder in folders)
            {
                Expand(folder);
            }
        }

        public bool IsExpanded(string name)
        {
            return !methods.IsElementExist(Selectors.ClosedArrowByXPath(name));
        }

        public bool IsFolderExist(string name)
        {
            return methods.IsElementExist(Selectors.LinkByXPath(name));
        }

        public CabinetPage OpenCabinet(string repository, string cabinet)
        {
            Expand(repository);
            methods.Click(Selectors.LinkByXPath(cabinet));
            return new CabinetPage();
        }

        public Folder OpenContainer(string containerName)
        {
            waits.WaitForPresenceOfAllElements(Selectors.LinkByXPath(containerName));
            methods.ScrollIntoView(Selectors.LinkByXPath(containerName));
            methods.Click(Selectors.LinkByXPath(containerName));
            return new Folder();
        }

        public sealed override void WaitForLoad()
        {
            waits.WaitForPresenceOfAllElements(Selectors.NavigationTreeByClass);
        }
    }

    public class NavigationPaneSelectors
    {
        public readonly By AddFolderByClass = By.ClassName("navAddF");

        public readonly By NavigationTreeByClass = By.ClassName("navTree");

        public By ClosedArrowByXPath(string name)
        {
            return By.XPath($"//a[text()=\'{name}\']//..//..//img[contains(@src,\'closed\')]");
        }

        public By LinkByXPath(string name)
        {
            return By.XPath($" //a[text()=\'{name}\']");
        }

        public By OpenArrowByXPath(string name)
        {
            return By.XPath($"//a[text()=\'{name}\']//..//..//img[contains(@src,\'open\')]");
        }
    }
}

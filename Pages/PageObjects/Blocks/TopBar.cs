﻿using OpenQA.Selenium;
using Pages.PageObjects.Pages;

namespace Pages.Pages.Blocks
{
    public class TopBar : APage<TopBarSelectors>
    {
        public TopBar(bool wait = true) : base(new TopBarSelectors())
        {
            if (wait)
            {
                WaitForLoad();
            }
        }

        public TopBar GoHome()
        {
            methods.Click(Selectors.HeaderLogo);
            return new TopBar();
        }

        public LogOutPage LogOut()
        {
            methods.Click(Selectors.PersonalMenu);

            methods.ClickViaJs(Selectors.Logout);
            //Click(TopBarSelectors.Logout); Does not work in FireFox Browser.

            return new LogOutPage();
        }

        public AdminPage OpenAdmin()
        {
            methods.Click(Selectors.PersonalMenu);

            methods.ClickViaJs(Selectors.Admin);
            //Click(TopBarSelectors.Admin); Does not work in FireFox Browser.

            return new AdminPage();
        }

        public sealed override void WaitForLoad()
        {
            waits.WaitForPresenceOfAllElements(Selectors.PersonalMenu);
        }
    }

    public class TopBarSelectors
    {
        public readonly By HeaderLogo = By.Id("headerLogoImg");

        public readonly By Logout = By.Id("ndLogOut");  
        public readonly By Admin = By.Id("ndAdmin"); 

        //public readonly By PersonalMenu = By.Id("personalMenu"); //div#personalMenu
        public readonly By PersonalMenu = By.CssSelector("div#personalMenu");// firefox
                                                                                         }
}

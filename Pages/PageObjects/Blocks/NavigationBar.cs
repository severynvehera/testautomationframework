﻿using OpenQA.Selenium;

namespace Pages.Pages.Blocks
{
    public class NavigationBar : APage<NavigationBarSelectors>
    {
        public NavigationBar(bool wait = true) : base(new NavigationBarSelectors())
        {
            if (wait)
            {
                WaitForLoad();
            }
        }

        public void CloseNavigationPane()
        {
            methods.Click(Selectors.NavigationPaneById);
        }

        public NavigationPane ExpandNavigationPane()
        {
            methods.Click(Selectors.NavigationPaneById);
            return new NavigationPane();
        }

        public sealed override void WaitForLoad()
        {
            waits.WaitForPresenceOfAllElements(Selectors.NavigationPaneById);
        }
    }

    public class NavigationBarSelectors
    {
        public readonly By NavigationPaneById = By.Id("nav-pane-button-li");
    }
}

﻿using OpenQA.Selenium;

namespace Pages.Pages.Blocks
{
    public class ItemDetailPane : APage<ItemDetailPaneSelectors>
    {
        public ItemDetailPane(bool wait = true) : base(new ItemDetailPaneSelectors())
        {
            if (wait)
            {
                WaitForLoad();
            }
        }

        public string ItemName => methods.GetText(Selectors.ItemNameById);

        public void CopyItemId()
        {
            methods.Click(Selectors.ItemIdById);
        }

        public sealed override void WaitForLoad()
        {
            waits.WaitForPresenceOfAllElements(Selectors.ItemIdById);
        }
    }

    public class ItemDetailPaneSelectors
    {
        public readonly By ItemIdById = By.Id("lv-docIDNum");

        public readonly By ItemNameById = By.Id("dp-itemName");
    }
}

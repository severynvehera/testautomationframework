﻿using OpenQA.Selenium;
using Pages.Pages.OptionsMenu;

namespace Pages.Pages.Blocks
{
    public class ContainerPowerBar : APage<ContainerPowerBarSelectors>
    {
        public ContainerPowerBar(bool wait = true) : base(new ContainerPowerBarSelectors())
        {
            if (wait)
            {
                WaitForLoad();
            }
        }

        public int GetItemsTotal()
        {
            return int.Parse(methods.GetText(Selectors.ItemsTotalByClass));
        }

        public MoreMenu OpenMoreOptionsMenu()
        {
            methods.Click(Selectors.MoreOptionsById);
            return new MoreMenu();
        }

        public sealed override void WaitForLoad()
        {
            waits.WaitForPresenceOfAllElements(Selectors.ContainerPowerBarBlockByClass);
        }
    }

    public class ContainerPowerBarSelectors
    {
        public readonly By ContainerPowerBarBlockByClass = By.ClassName("container-powerbar");

        public readonly By ItemsTotalByClass = By.ClassName("total");

        public readonly By MoreOptionsById = By.Id("more");
    }
}

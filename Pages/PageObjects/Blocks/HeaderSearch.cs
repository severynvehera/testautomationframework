﻿using OpenQA.Selenium;
using Pages.PageObjects.Pages;
using Pages.Pages.Containers;

namespace Pages.Pages.Blocks
{
    public class HeaderSearch : APage<HeaderSearchSelectors>
    {
        public HeaderSearch(bool wait = true) : base(new HeaderSearchSelectors())
        {
            if (wait)
            {
                WaitForLoad();
            }
        }

        public void ChangeScope(string cabinet = "All Cabinets")
        {
            methods.Click(Selectors.ChangeScopeButtonById);
            methods.Click(Selectors.SearchWithin(cabinet));
        }

        public AdvancedSearchPage OpenAdvancedSearch()
        {
            methods.Click(Selectors.AdvancedSearchButtonById);
            return new AdvancedSearchPage();
        }

        public SearchResults SearchFor(string text)
        {
            methods.Click(Selectors.SearchFieldById);
            methods.EnterText(Selectors.SearchFieldById, text);
            methods.Click(Selectors.SearchButtonById);

            return new SearchResults();
        }

        public sealed override void WaitForLoad()
        {
            waits.WaitForPresenceOfAllElements(Selectors.SearchFieldById);
        }
    }

    public class HeaderSearchSelectors
    {
        public readonly By AdvancedSearchButtonById = By.Id("hsAdv");

        public readonly By ChangeScopeButtonById = By.Id("hsCabs");

        public readonly By SearchButtonById = By.Id("hsGo");

        public readonly By SearchFieldById = By.Id("nd-hsCriteria-input");

        public  By SearchWithin(string name)
        {
            return By.XPath($"//div//span[text()=\"{name}\"]");
        }
    }
}

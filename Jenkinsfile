pipeline {
  agent any
  environment{
      DEBUG_DIR = "${env.WORKSPACE}/Tests/bin/Debug"
      NUNIT_CONSOLE = "${env.WORKSPACE}/packages/NUnit.ConsoleRunner.3.9.0/tools/nunit3-console.exe"
      TESTS_DLL = "${env.DEBUG_DIR}/Tests.dll"
      TESTS_LOG = "${env.DEBUG_DIR}/ndlog.log"
      TEST_RESULT_OUTPUT = "TestResults.xml"

      CHROME_TEST_RESULT_FOLDER = "Chrome_TestResults"
      FIREFOX_TEST_RESULT_FOLDER = "Firefox_TestResults"
      EDGE_TEST_RESULT_FOLDER = "Edge_TestResults"
      IE_TEST_RESULT_FOLDER = "IE_TestResults"
  }
  
  stages {
    stage('Restore packages') {
      steps {
        bat 'nuget restore'
      }
    }
    stage('Build') {
      steps {
        bat "\"${tool 'MSBuild'}\" TestFramework.sln /p:Configuration=Debug /p:Platform=\"Any CPU\" /p:ProductVersion=1.0.0.${env.BUILD_NUMBER}"
        dir("${DEBUG_DIR}"){
          bat "mkdir ${env.CHROME_TEST_RESULT_FOLDER}"
          bat "mkdir ${env.FIREFOX_TEST_RESULT_FOLDER}"
          bat "mkdir ${env.EDGE_TEST_RESULT_FOLDER}"
          bat "mkdir ${env.IE_TEST_RESULT_FOLDER}"
        }
      }
    }
    
    stage('Tests') {
      
      input {
        message "Tests Set Up"
        ok "Run tests"
        parameters {
          choice(choices: ['False','True'], description: 'Chrome: run on remote?', name: 'CHROME')
          choice(choices: ['False','True'], description: 'Firefox: run on remote?', name: 'FIREFOX')
          choice(choices: ['False','True'], description: 'Edge: run on remote?', name: 'EDGE')
          choice(choices: ['False','True'], description: 'Internet Explorer: run on remote?', name: 'IE')
          choice(choices: ['All tests', 'LoginTest', 'NetDocumentsTests','LitmusTest'], description: 'Fixture you are running?', name: 'FIXTURE')
        }
      }
     
      parallel {

        stage("Test on Chrome") {
          steps {
             dir("${DEBUG_DIR}/${env.CHROME_TEST_RESULT_FOLDER}"){
              script {
                if ("${FIXTURE}" == 'All tests') {
                  bat   "\"${env.NUNIT_CONSOLE}\" \"${env.TESTS_DLL}\" --params \"browser=Chrome;remote=${CHROME}\" /result:${env.TEST_RESULT_OUTPUT}"
                } else {
                  bat "\"${env.NUNIT_CONSOLE}\" \"${env.TESTS_DLL}\" --where \"class == Tests.TestClasses.${FIXTURE}\" --params \"browser=Chrome;remote=${CHROME}\" /result:\"Chrome_${env.TEST_RESULT_OUTPUT}\""
                }
              }
             }
          }

          post { 
            always {
             dir("${DEBUG_DIR}/${env.CHROME_TEST_RESULT_FOLDER}"){
               xunit([
                 NUnit3(
                   deleteOutputFiles: true, 
                   failIfNotNew: true, 
                   pattern: "Chrome_${env.TEST_RESULT_OUTPUT}",
                   skipNoTestFiles: false, 
                   stopProcessingIfError: true)
                ])

                archiveArtifacts "Chrome_${env.TEST_RESULT_OUTPUT}"
                allure includeProperties: false, jdk: '', results: [[path: "allure-results"]]
                publishHTML([
                  allowMissing: false, 
                  alwaysLinkToLastBuild: false, 
                  keepAll: true, 
                  reportDir: "${DEBUG_DIR}/${env.CHROME_TEST_RESULT_FOLDER}/allure-report", 
                  reportFiles: 'index.html', 
                  reportName: 'ChromeReports', 
                  reportTitles: 'Chrome'
                  ])
              }
            }
          } 
        }

        stage("Test on Firefox") {
          steps {
            dir("${DEBUG_DIR}/${env.FIREFOX_TEST_RESULT_FOLDER}"){
              script {
                if ("${FIXTURE}" == 'All tests') {
                  bat "\"${env.NUNIT_CONSOLE}\" \"${env.TESTS_DLL}\" --params \"browser=Firefox;remote=${FIREFOX}\" /result:${env.TEST_RESULT_OUTPUT}"
                } else {
                  bat "\"${env.NUNIT_CONSOLE}\" \"${env.TESTS_DLL}\" --where \"class == Tests.TestClasses.${FIXTURE}\" --params \"browser=Firefox;remote=${FIREFOX}\" /result:\"Firefox_${env.TEST_RESULT_OUTPUT}\""
                }
              }
            }
          }

          post { 
            always {
              dir("${DEBUG_DIR}/${env.FIREFOX_TEST_RESULT_FOLDER}"){
                 xunit([
                 NUnit3(
                   deleteOutputFiles: true, 
                   failIfNotNew: true, 
                   pattern: "Firefox_${env.TEST_RESULT_OUTPUT}",
                   skipNoTestFiles: false, 
                   stopProcessingIfError: true)
                ])

                archiveArtifacts "Firefox_${env.TEST_RESULT_OUTPUT}"
                allure includeProperties: false, jdk: '', results: [[path: "allure-results"]]
               
                publishHTML([
                  allowMissing: false, 
                  alwaysLinkToLastBuild: false, 
                  keepAll: true, 
                  reportDir: "${DEBUG_DIR}/${env.FIREFOX_TEST_RESULT_FOLDER}/allure-report", 
                  reportFiles: 'index.html', 
                  reportName: 'FirefoxReports', 
                  reportTitles: 'Firefox'
                  ])
              }
            }
          } 
        }

        stage("Test on Edge") {
          steps {
            dir("${DEBUG_DIR}/${env.EDGE_TEST_RESULT_FOLDER}"){
              script {
                if ("${FIXTURE}" == 'All tests') {
                  bat "\"${env.NUNIT_CONSOLE}\" \"${env.TESTS_DLL}\" --params \"browser=Edge;remote=${EDGE}\" /result:${env.TEST_RESULT_OUTPUT}"
                } else {
                  bat "\"${env.NUNIT_CONSOLE}\" \"${env.TESTS_DLL}\" --where \"class == Tests.TestClasses.${FIXTURE}\" --params \"browser=Edge;remote=${EDGE}\" /result:\"Edge_${env.TEST_RESULT_OUTPUT}\""
                }
              }
            }
          }

          post { 
            always {
              dir("${DEBUG_DIR}/${env.EDGE_TEST_RESULT_FOLDER}"){
                xunit([
                 NUnit3(
                   deleteOutputFiles: true, 
                   failIfNotNew: true, 
                   pattern: "Edge_${env.TEST_RESULT_OUTPUT}",
                   skipNoTestFiles: false, 
                   stopProcessingIfError: true)
                ])

                archiveArtifacts "Edge_${env.TEST_RESULT_OUTPUT}"
                allure includeProperties: false, jdk: '', results: [[path: "allure-results"]], report : "Chrome"
               publishHTML([
                  allowMissing: false, 
                  alwaysLinkToLastBuild: false, 
                  keepAll: true, 
                  reportDir: "${DEBUG_DIR}/${env.EDGE_TEST_RESULT_FOLDER}/allure-report", 
                  reportFiles: 'index.html', 
                  reportName: 'EdgeReports', 
                  reportTitles: 'Edge'
                  ])
              }
            }
          } 
        }

        stage("Test on IE") {
           steps {
              dir("${DEBUG_DIR}/${env.IE_TEST_RESULT_FOLDER}"){
              script {
                if ("${FIXTURE}" == 'All tests') {
                  bat "\"${env.NUNIT_CONSOLE}\" \"${env.TESTS_DLL}\" --params \"browser=IE;remote=${IE}\" /result:${env.TEST_RESULT_OUTPUT}"
                } else {
                  bat "\"${env.NUNIT_CONSOLE}\" \"${env.TESTS_DLL}\" --where \"class == Tests.TestClasses.${FIXTURE}\" --params \"browser=IE;remote=${IE}\" /result:\"IE_${env.TEST_RESULT_OUTPUT}\""
                }
              }
            }
          }

          post { 
            always {
             dir("${DEBUG_DIR}/${env.IE_TEST_RESULT_FOLDER}"){
                xunit([
                 NUnit3(
                   deleteOutputFiles: true, 
                   failIfNotNew: true, 
                   pattern: "IE_${env.TEST_RESULT_OUTPUT}",
                   skipNoTestFiles: false, 
                   stopProcessingIfError: true)
                ])

                archiveArtifacts "IE_${env.TEST_RESULT_OUTPUT}"
                allure includeProperties: false, jdk: '', results: [[path: "allure-results"]]
                 publishHTML([
                  allowMissing: false, 
                  alwaysLinkToLastBuild: false, 
                  keepAll: true, 
                  reportDir: "${DEBUG_DIR}/${env.IE_TEST_RESULT_FOLDER}/allure-report", 
                  reportFiles: 'index.html', 
                  reportName: 'IE_Reports', 
                  reportTitles: 'IE'
                  ])
              }
            }
          } 
        }
        
      }
         
      post { 
        always {
          archiveArtifacts artifacts: "${env.TESTS_LOG}", fingerprint: false
        }
      }  
    }
  }
}
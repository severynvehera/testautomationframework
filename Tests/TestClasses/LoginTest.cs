﻿using NUnit.Framework;
using System.Collections;
using NUnit.Allure.Core;
using TestFramework.Utils;

namespace Tests.TestClasses
{
    [TestFixture]
    [AllureNUnit]
    public class LoginTest : BaseTest
    {
        const string expectedMassage = "Your previous login attempt failed. Please try again.";

        public static IEnumerable LoginNegativeCases
        {
            get
            {
                    yield return new TestCaseData("", "").Returns(expectedMassage);
                    yield return new TestCaseData("@$$^^%#%@%", "").Returns(expectedMassage);
                    yield return new TestCaseData("atqa.test2018", "").Returns(expectedMassage);
                    yield return new TestCaseData("", "rewards4").Returns(expectedMassage);
            }
        }

        [SetUp]
        public void BeforeTest()
        {
            TestParameters.Browser = "Firefox";
            // Pages.Stub.LogOutIfLoggedIn();
        }

        [TestCaseSource(nameof(LoginNegativeCases))]
        public string LoginNegative(string username, string password)
        {
            Page.Login.LoginUser(username, password, false);
            Page.Login.WaitForErrorMessage();
            return Page.Login.GetErrorMessage();
        }

        [Test]
        public void LoginPositive()
        {
            Page.Login.LoginUser(TestParameters.Username);
            Page.TopBar.LogOut().OpenLoginPage();
        }
    }
}

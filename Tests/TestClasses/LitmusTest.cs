﻿using NUnit.Framework;
using System;
using NUnit.Allure.Core;
using NUnit.Allure.Attributes;
using Allure.Commons;

namespace Tests.TestClasses
{
    [TestFixture]
    [AllureNUnit]
    public class LitmusTest : BaseTest
    {
        [Test]
        public void ErrorTest()
        {
            throw new NotSupportedException();
        }

        [Test(Description = "Sample")]
        [AllureTag("TC-1")]
        [AllureSeverity(SeverityLevel.critical)]
        [AllureIssue("ISSUE-1")]
        [AllureTms("TMS-12")]
        [AllureOwner("unickq")]
        [AllureSuite("PassedSuite")]
        [AllureSubSuite("NoAssert")]
        [AllureSubSuite("Simple")]
        public void FailTest()
        {
            Assert.True(false);
        }

        [Test]
        [Ignore("Skipped using the \"Ignore\" attribute")]
        public void IgnoreTest()
        {
            Console.WriteLine();
        }

        [Test]
        public void InconclusiveTest()
        {
            Assert.Inconclusive();
        }

        [Test]
        public void SkipTest()
        {
            Assert.Ignore();
        }

        [Test]
        public void SuccessTest()
        {
            Assert.True(true);
        }
    }
}

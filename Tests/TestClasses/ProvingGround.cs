﻿using NUnit.Allure.Attributes;
using NUnit.Allure.Core;
using NUnit.Framework;
using System.Diagnostics;
using TestFramework.Utils;

namespace Tests.TestClasses
{
    [TestFixture]
    [AllureNUnit]
    public class ProvingGround : BaseTest
    {
        [OneTimeSetUp]
        public void Login()
        {
            Page.Login.LoginUser(TestParameters.Username);
        }

        private readonly string repository = "aqa test repo";
        private readonly string cabinet = "test cab";
        private readonly string folderNameID = $"Folder_{Stopwatch.GetTimestamp()}";

        [Test(Author = "unickq")]
        [Description("OLOLO")]
        [AllureSuite("FailedSuite")]
        [AllureSubSuite("DoesNotThrow")]
        [AllureLink("Google", "https://google.com")]
        public void CreateFolderViaNavPane1()
        {
            string folderName = "folder";

            Page.NavigationBar.ExpandNavigationPane();

            Page.NavigationPane.AddTopLevelFolder(repository, cabinet);

            Page.CreateNewFolderPage.CreateNewFolder(folderName);

            Page.NavigationBar.ExpandNavigationPane();

            Page.NavigationPane.ExpandFolders(repository, cabinet, folderName);

            Assert.True(Page.NavigationPane.IsFolderExist(folderName));

            Page.NavigationBar.CloseNavigationPane();
        }

        [Test, Order(1)]
        public void CreateFolderViaNavPane2()
        {
            string folderName = "folder";

            Page.NavigationBar.ExpandNavigationPane();

            Page.NavigationPane.AddTopLevelFolder(repository, cabinet);

            Page.CreateNewFolderPage.CreateNewFolder(folderName);

            Page.NavigationBar.ExpandNavigationPane();

            Page.NavigationPane.ExpandFolders(repository, cabinet, folderName);

            Assert.True(Page.NavigationPane.IsFolderExist(folderName));

            Page.NavigationBar.CloseNavigationPane();
        }

        [Test, Order(2)]
        public void CreateFolderViaNavPane3()
        {
            string folderName = folderNameID;

            Page.NavigationBar.ExpandNavigationPane();

            Page.NavigationPane.AddTopLevelFolder(repository, cabinet);

            Page.CreateNewFolderPage.CreateNewFolder(folderName);

            Page.NavigationBar.ExpandNavigationPane();

            Page.NavigationPane.ExpandFolders(repository, cabinet, folderName);

            Assert.True(Page.NavigationPane.IsFolderExist(folderName));

            Page.NavigationBar.CloseNavigationPane();
        }

    }
}
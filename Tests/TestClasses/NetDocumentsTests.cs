﻿using NUnit.Framework;
using OpenQA.Selenium;
using Pages.Pages.OptionsMenu;
using System.Diagnostics;
using NUnit.Allure.Core;
using TestFramework.Utils;

namespace Tests.TestClasses
{
    [TestFixture]
    [AllureNUnit]
    public class NetDocumentsTests : BaseTest
    {
        [OneTimeSetUp]
        public void Login()
        {
            //Pages.Stub.LogOutIfLoggedIn();
            Page.Login.LoginUser(TestParameters.Username);
        }

        [TestCaseSource(typeof(DataClass), "CreateFolderFromCabinetCases")]
        public void CreateFolderFromCabinet(string repository, string cabinet, string folderName)
        {
            Page.NavigationBar.ExpandNavigationPane();
            Page.NavigationPane.OpenCabinet(repository, cabinet);

            Page.Cabinet.OpenCabinetOptions().SelectItem(CabinetMenuItems.AddFolder);

            Page.CreateNewFolderPage.CreateNewFolder(folderName);
            Page.NavigationBar.ExpandNavigationPane();

            Page.NavigationPane.ExpandFolders(repository, cabinet, folderName);

            Assert.True(Page.NavigationPane.IsFolderExist(folderName));

            Page.NavigationBar.CloseNavigationPane();
        }

        [TestCaseSource(typeof(DataClass), "CreateFolderCases")]
        public void CreateFolderViaNavPane(string repository, string cabinet, string folderName)
        {
            Page.NavigationBar.ExpandNavigationPane();

            Page.NavigationPane.AddTopLevelFolder(repository, cabinet);

            Page.CreateNewFolderPage.CreateNewFolder(folderName);

            Page.NavigationBar.ExpandNavigationPane();

            Page.NavigationPane.ExpandFolders(repository, cabinet, folderName);

            Assert.True(Page.NavigationPane.IsFolderExist(folderName));

            Page.NavigationBar.CloseNavigationPane();
        }

        [TestCaseSource(typeof(DataClass), "DeleteFolderCases")]
        public void DeleteFolderViaContainer(string repository, string cabinet, string folderName)
        {
            Page.NavigationBar.ExpandNavigationPane();

            Page.NavigationPane.ExpandFolders(repository, cabinet, folderName);

            Page.NavigationPane.OpenContainer(folderName);

            Page.FolderContainer.OpenContainerMenu()
                .SelectItem(ContainerMenuItems.DeleteFolder);

            Page.DeletePopUp.Delete();

            Page.Cabinet.WaitForLoad();

            Page.NavigationBar.ExpandNavigationPane();

            Page.NavigationPane.ExpandFolders(repository, cabinet, folderName);
            //Pages.NavigationPane.ExpandFolders(repository, cabinet);

            Page.NavigationPane.OpenContainer(folderName);
            //Assert.False(Pages.NavigationPane.IsFolderExist(folderName), $"The {folderName} still presence in navigation pane");
            Assert.AreEqual("This folder is deleted.", Page.FolderContainer.EmptyMessageText);
            //Pages.NavigationBar.CloseNavigationPane();
        }

        [TestCaseSource(typeof(DataClass), "PermanentlyDeleteFolderCases")]
        public void PermanentlyDeleteViaMoreOptions(string folderName)
        {
            Page.HeaderSearch.ChangeScope();

            Page.HeaderSearch.SearchFor(folderName);

            AssertCheckIndexing(folderName);

            Page.SearchResultsContainer.ClickCheckbox(folderName).CopyItemId();

            Page.ContainerPowerBar.OpenMoreOptionsMenu()
                .SelectItem(MoreMenuItems.Delete);

            Page.DeleteDialog.Delete();

            Page.HeaderSearch.OpenAdvancedSearch();

            Page.AdvancedSearch.ShowMoreOptions();

            Page.AdvancedSearch.SearchDocumentById();

            AssertCheckIndexing(folderName);

            Page.SearchResultsContainer.Open(folderName);

            Page.FolderContainer.OpenContainerMenu()
                .SelectItem(ContainerMenuItems.DeleteFolder);

            Page.DeletePopUp.Delete();

            // check
            Page.HeaderSearch.ChangeScope();

            Page.HeaderSearch.SearchFor($"{Keys.Control}v");

            Assert.AreEqual("There are no documents that match these criteria or you may not have access to them.", Page.SearchResultsContainer.EmptyMessageText);
        }

        [TestCaseSource(typeof(DataClass), "RenameFolderCases")]
        public void RenameFolderFromSearchPageViaContextMenu(string folderName, string newFolderName)
        {
            Page.HeaderSearch.ChangeScope();

            Page.HeaderSearch.SearchFor(folderName);

            AssertCheckIndexing(folderName);

            Page.SearchResultsContainer.OpenContextMenu(folderName)
                .SelectItem(ContextMenuItems.Rename);

            Page.RenameDialog.NewName(newFolderName);

            Assert.True(Page.SearchResultsContainer.IsItemExist(newFolderName), $"{newFolderName} is not exist");
            Assert.AreEqual(newFolderName, Page.ItemDetailPane.ItemName, $"{newFolderName} is not exist");
        }

        private void AssertCheckIndexing(string itemName)
        {
            Assert.True(Page.SearchResultsContainer.CheckIndexing(itemName), $"{itemName} is not found");
        }
    }

    internal static class DataClass
    {
        private static readonly string folderName = $"Folder_{Stopwatch.GetTimestamp()}";

        private static readonly object[] CreateFolderCases =
        {
            new object[] {"aqa test repo", "test cab", folderName}
        };

        private static readonly object[] CreateFolderFromCabinetCases =
        {
            new object[] {"aqa test repo", "test cab", $"{folderName}c"},
            new object[] {"aqa test repo", "test cab", $"{folderName}_For_Rename"}
        };

        private static readonly object[] DeleteFolderCases =
        {
            new object[] {"aqa test repo", "test cab", $"{folderName}c" }
        };

        private static readonly object[] PermanentlyDeleteFolderCases =
        {
            new object[] {$"{folderName}"}
        };

        private static readonly object[] RenameFolderCases =
        {
            new object[] {$"{folderName}_For_Rename", $"{folderName}_Renamed"}
        };
    }
}

﻿using NUnit.Framework;
using Pages.Pages.OptionsMenu;

namespace Tests.TestClasses
{
    // [TestFixture]
    public class NetDocumentsNegativeTests : BaseTest
    {
        //[Test]
        public void CreateFolderFromCabinetNegative()
        {
            Page.NavigationBar.ExpandNavigationPane().OpenCabinet("aqa test repo", "test cab").OpenCabinetOptions().SelectItem(CabinetMenuItems.AddFolder);
            Page.CreateNewFolderPage.CreateNewFolder("");
            Page.Waits().WaitForAlert();
            //Pages.CreateNewFolderPage.WaitForAlert();
            Assert.True(Page.Window().VerifyAlertText("A name is required."));
            Page.Window().CloseAlert();
        }

        //[Test]
        public void CreateFolderViaNavPaneNegative()
        {
            Page.NavigationBar.ExpandNavigationPane();

            Page.NavigationPane.AddTopLevelFolder("aqa test repo", "test cab");

            Page.CreateNewFolderPage.CreateNewFolder("");
            Page.Waits().WaitForAlert();
            //Pages.CreateNewFolderPage.WaitForAlert();

            Assert.True(Page.Window().VerifyAlertText("A name is required."));

            Page.Window().CloseAlert();
        }

        // [Test]
        public void RenameFolderFromSearchPageViaContextMenuNegative()
        {
            Page.HeaderSearch.SearchFor("test").OpenContextMenu("test1").SelectItem(ContextMenuItems.Rename);
            Page.SearchResultsContainer.CheckIndexing("test1");
            Page.RenameDialog.NewName("");
            Page.Waits().WaitForAlert();
            //Pages.RenameDialog.WaitForAlert();
            Assert.True(Page.Window().VerifyAlertText("Please enter a new name for the folder."));
            Page.Window().CloseAlert();
            Page.RenameDialog.Cancel();
        }
    }
}

﻿using NUnit.Framework;
using System.Collections.Generic;
using TestFramework.Utils;

namespace Tests
{
    /// <summary>
    /// This class retrieves test parameters from nunit3-console or App.config
    /// </summary>
    public class TestParameters
    {
        public static Dictionary<string, string> ParamByName = new Dictionary<string,string>();

        private readonly string BrowserKey = "browser";
        private readonly string BaseUrlKey = "base.url";
        private readonly string UsernameKey = "username";
        private readonly string PasswordKey = "password";
        private readonly string RemoteKey = "remote";

        private string _browser;
        private string _baseUrl;
        private string _username;
        private string _password;
        private string _remote;

        public string Browser
        {
            get => _browser ?? (_browser = AssignTestParameter(BrowserKey));
            set => _browser = value;
        }

        public string BaseUrl
        {
            get => _baseUrl ?? (_baseUrl = AssignTestParameter(BaseUrlKey));
            set => _baseUrl = value;
        }

        public string Username
        {
            get => _username ?? (_username = AssignTestParameter(UsernameKey));
            set => _username = value;
        }

        public string Password
        {
            get => _password ?? (_password = AssignTestParameter(PasswordKey));
            set => _password = value;
        }

        public string Remote
        {
            get => _remote ?? (_remote = AssignTestParameter(RemoteKey));
            set => _remote = value;
        }

        /// <summary>
        /// Assigns the value to the test parameter. If the value is not passed as the parameter of 'nunit3-console.exe' then 
        /// uses the default value from the App.config
        /// </summary>
        /// <param name="parameterKey">The key name in the App.config.</param>
        private string AssignTestParameter(string parameterKey)
        {
            if (TestContext.Parameters.Exists(parameterKey))
            {
                return TestContext.Parameters[parameterKey];
            }
            else
            {
                return GlobalTools.GetValueOf(parameterKey);
            }
        }
    }
}
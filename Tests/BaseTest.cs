﻿using log4net;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using Pages;
using System;
using System.Diagnostics;
using System.IO;
using Allure.Commons;
using TestFramework;
using TestFramework.Utils;

namespace Tests
{
    //The [SetUpFixture] attribute calls the method marked as OneTimeSetUp 3 times.
    [TestFixture]
    public abstract class BaseTest
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(BaseTest));

        protected PagesSingleton Page { get; } = PagesSingleton.Instance;

        protected TestParameters TestParameters => new TestParameters();
        private Browser browser;
        private bool remote;

        [OneTimeSetUp]
        public void SetUpEnvironment()
        {
            browser = GlobalTools.ParseBrowserName(TestParameters.Browser);
            remote = bool.Parse(TestParameters.Remote);

            ApplicationSetUpSingleton.Initilize(browser, remote);
            //AllureLifecycle.Instance.CleanupResultDirectory();
            log.Info("Starting One time set up");

            Page.Window().InitialWindow = Page.Window().CurrentWindow;
            Page.Window().MaximizeWindow();

            Page.Browser().GoToUrl(TestParameters.BaseUrl);
            log.Info("One time set up complete");
            
            Tests.TestParameters.ParamByName.Add("Browser", TestParameters.Browser);
            Tests.TestParameters.ParamByName.Add("Base Url", TestParameters.BaseUrl);
            Tests.TestParameters.ParamByName.Add("Remote", TestParameters.Remote);
        }

        [OneTimeTearDown]
        public void TearDownEnvironment()
        {
            GlobalTools.CreateEnvironmentXml(Tests.TestParameters.ParamByName);

            log.Info("Starting One time tear down");
            ApplicationSetUpSingleton.TearDown();
            log.Info("One time tear down complete\n");
        }
        /// <summary>
        /// Takes screenshot only if test ResultState - FAILURE. Attaches file to TestContext
        /// </summary>
        [TearDown]
        public void ScreenshotAtFailure()
        {
            if ((TestContext.CurrentContext.Result.Outcome != ResultState.Failure)) return ;

            var screenshot = Page.Window().TakeScreenshot();
            string root = $@"{AppContext.BaseDirectory}Screenshots\{TestContext.CurrentContext.Test.ClassName}\";
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
            string fileName = $"{root}{TestContext.CurrentContext.Test.MethodName}_{Stopwatch.GetTimestamp()}.jpeg";

            screenshot.SaveAsFile(fileName);
            TestContext.AddTestAttachment(fileName);
            AllureLifecycle.Instance.AddAttachment(fileName);
        }
    }
}
